$(document).ready(function() {
	$("#add-project-button").click(function() {
		$("#add-project-form").submit();
		return false;
	});
	$(".delete-project-button").click(function() {
		if (confirm("Вы действительно хотите удалить данный проект?")) {
			var projectID = $(this).attr("data-id");
			$("#delete-project input[name=project-id]").val(projectID);
			$("#delete-project").submit();
		}
		return false;
	});
	$(".edit-project-button").click(function() {
		var projectID = $(this).attr("data-id");
		var rowBlock = $(this).parent().parent();
		$("#edit-project input[name=project-name]").val(rowBlock.find(".project-name-tag").text());
		$("#edit-project input[name=project-url]").val(rowBlock.find(".project-url-tag").text());
		$("#edit-project input[name=project-id]").val(projectID);
		$("#edit-project").modal('show');
		return false;
	});
	$("#edit-project-button").click(function() {
		$("#edit-project-form").submit();
		return false;
	});
	
	$("#reload-categories-button").click(function() {
		$("#reload-categories").submit();
		return false;
	});
	
	$("#push-article-request-button").click(function() {
		$("#push-article-request").submit();
		return false;
	});
	
	$("#check-article-status-button").click(function() {
		$("#check-article-status").submit();
		return false;
	});
	
	$("#approve-article-button").click(function() {
		$("#approve-article").submit();
		return false;
	});
	
	$("#check-articles-status-button").click(function() {
		$("#check-articles-status").submit();
		return false;
	});
        // Templates:
        $(".delete-template-button").click(function() {
            if (confirm("Вы действительно хотите удалить данный шаблон?")) {
                var templateID = $(this).attr("data-id");
                $("#delete-template input[name=id]").val(templateID);
                $("#delete-template").submit();
            }
            return false;
        });
        $("#add-var-button").click(function() {
            var htmlBlock = "<div class='well well-sm'><div class='form-group'><input type='text' name='var-description[]' class='form-control' placeholder='Enter var description...'></div><div class='form-group'><input name='var-name[]' type='text' class='form-control' placeholder='Enter var name...'></div></div>";
            $("#vars-panel").prepend(htmlBlock);
            return false;
        });
        $(".open-template").click(function() {
            $(this).parent().next().toggle(200);
            return false;
        });
        // Project:
        $(".delete-article-button").click(function() {
            if (confirm("Вы действительно хотите удалить данную статью?")) {
                var articleID = $(this).attr("data-id");
                $("#delete-article input[name=article-id]").val(articleID);
                $("#delete-article").submit();
            }
        });
        $("#move-to-archive-button").click(function() {
             if (confirm("Вы действительно хотите переместить проект в архив?")) {
                 $("#move-to-archive").submit();
             }
        });
        $("#move-from-archive-button").click(function() {
             if (confirm("Вы действительно хотите вернуть проект из архива?")) {
                 $("#move-from-archive").submit();
             }
        });
});