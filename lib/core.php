<?php

class ArticleService {

	public static function get_balance() {
		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>get_balance</func_name>
</request>";
		
		$xml_response = new SimpleXMLElement(self::make_request());
		return $xml_response->balance;
	}
	
	public static function get_categories() {
		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>get_categories</func_name>
</request>";

		$xml_response = new SimpleXMLElement(self::make_request());
		if ($xml_response->status == 'ok') {
			$category_array = array();
			foreach ($xml_response->category as $category) {
				$category_id = (string)$category->id;
				$category_name = (string)$category->title;
				$category_array[$category_id] = $category_name;
			}
			return $category_array;
		}
		
		return false;
	}
	
	public static function get_project_status($project_id) {
	
		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>get_project_status</func_name>
	<proj_id>$project_id</proj_id>
</request>";
		
		if ($http_response = self::make_request()) {
			$xml_response = new SimpleXMLElement($http_response);
			if ($xml_response->status == 'ok') {
				$response_array['status'] = (string)$xml_response->keyword->status;
				// Review URL:
				if ($response_array['status'] == "available") {
					$response_array['article_id'] = (string)$xml_response->keyword->article_id;
					$response_array['review_url'] = (string)$xml_response->keyword->review_url;
				}
				return $response_array;
			}
		}
		
		return false;
	}
	
	// Approve article:
	public static function approve_article($service_project_id, $service_article_id, $comment) {
		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>review_article</func_name>
	<proj_id>$service_project_id</proj_id>
	<article_id>$service_article_id</article_id>
	<operation>approve</operation>
	<rate>5</rate>
	<comment>$comment</comment>
</request>";

		if ($http_response = self::make_request()) {
			$xml_response = new SimpleXMLElement($http_response);
			if ($xml_response->status == 'ok') {
				return true;
			}
		}
		return false;
	}
	
	// Download article:
	public static function download_article($service_project_id) {
		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>download_articles</func_name>
	<proj_id>$service_project_id</proj_id>
</request>";

		if ($http_response = self::make_request()) {
			$xml_response = new SimpleXMLElement($http_response);
			if ($xml_response->status == 'ok') {
				$article_array['title'] = (string)$xml_response->article->title;
				$article_array['text'] = (string)$xml_response->article->body;
				return $article_array;
			}
		}
		return false;
	}
	
	// Push article request:
	public static function push_article_request($article) {
	
		switch ($article->get_writers_type()) {
			case "ALL":
				$submit_tier = '';
				break;
			case "PREMIUM":
				$submit_tier = 'top';
				break;
			case "ELITE":
				$submit_tier = 'elite';
				break;
		}
		
		switch ($article->get_text_style()) {
			case "FRIENDLY":
				$text_type = 1;
				break;
			case "PROFESSIONAL":
				$text_type = 2;
				break;
		}
		
		// Keywords:
		$keywords_array = explode("\n", $article->get_keywords());
		$xml_keywords = '';
		foreach ($keywords_array as $keyword)  {
			$xml_keywords .= "<keyword>".trim($keyword)."</keyword>\n";
		}
		
		$writers_list = '';
		if ($article->get_writers_list()) {
			$writers_list = "<to_writers>".$article->get_writers_list()."</to_writers>";
		}

		self::$xml_request = "
<request>
	<user_id>".self::$api_id."</user_id>
	<api_key>".self::$api_key."</api_key>
	<func_name>add_project</func_name>
	<title>".$article->get_project_description()."</title>
	<category>".$article->get_category()."</category>
	<art_len>".$article->get_length()."</art_len>
	<submit_tier>$submit_tier</submit_tier>
	<price_per>".$article->get_price()."</price_per>
	<keywords>
		$xml_keywords
	</keywords>
	<writing_style>$text_type</writing_style>
	<art_purpose>".$article->get_article_purpose()."</art_purpose>
	<special_instructions>".$article->get_special_instructions()."</special_instructions>
	$writers_list
</request>";

		if ($http_response = self::make_request()) {
			$xml_response = new SimpleXMLElement($http_response);
			if ($xml_response->status == 'ok') {
				$current_time = time();
				$operation_status = mysql_query("UPDATE articles SET service_project_id='".$xml_response->project_id."', status='open', check_time='$current_time' WHERE id='".$article->get_id()."'");
				if ($operation_status) return true;
			}
		}
		
		return false;
	}
	
	private static function make_request() {
		$http = new HTTPClient(self::$api_url);
		$http->set_post_method(self::$xml_request);
		if ($response = $http->do_request()) {
			$http->close_client();
			return $response;
		}
		return false;
	}

	private static $api_url = "http://www.iwriter.com/api/";
	private static $api_id = "Steadfast";
	private static $api_key = "NDcyM2Q5MzMzMjI3ZTQ4ZDA4YmQzMD";
	private static $xml_request;
}

class SmartDB {
	// Get categories:
	public static function get_categories() {
		$cetegories_result = mysql_query("SELECT * FROM categories");
		$categories_array = array();
		while ($category = mysql_fetch_array($cetegories_result)) {
			$categories_array[] = $category;
		}
		return $categories_array;
	}
	
	// Update categories:
	public static function update_categories($categories) {
	
		if (is_array($categories) && count($categories) > 0) {
                    
			// Delete categories:
			$operation_result = mysql_query("DELETE FROM categories");
			if (!$operation_result) return false;
			
			foreach($categories as $id => $name) {
				mysql_query("INSERT INTO categories SET service_id='$id', name='$name'");
			}
			
			return true;
		}
	}

	// Get all projects:
	public static function get_projects() {
		$projects_result = mysql_query("SELECT * FROM projects WHERE archived='0'");
		$projects_array = array();
                echo mysql_error();
		while ($project = mysql_fetch_array($projects_result)) {
			$projects_array[] = new Project($project);
		}
		return $projects_array;
	}
        
	// Get all archive projects:
	public static function get_archive_projects() {
		$projects_result = mysql_query("SELECT * FROM projects WHERE archived='1'");
		$projects_array = array();
		while ($project = mysql_fetch_array($projects_result)) {
			$projects_array[] = new Project($project);
		}
		return $projects_array;
	}
	
	// Add new project:
	public static function add_project($project) {
		$operation_result = false;
		if ($project->get_name()) {
			if (mysql_query("INSERT INTO projects SET name='".$project->get_name()."', url='".$project->get_url()."'"))
				return true;
		}
		return false;
	}
	
	// Update project:
	public static function update_project($project) {
		$operation_result = false;
		if (is_numeric($project->get_id())) {
			if (mysql_query("UPDATE projects SET name='".$project->get_name()."', url='".$project->get_url()."' WHERE id='".$project->get_id()."'"))
				return true;
		}
		return false;
	}
	
	// Delete project:
	public static function delete_project($project) {
		$first_operation = mysql_query("DELETE FROM projects WHERE id='".$project->get_id()."'");
		$second_operation = mysql_query("DELETE FROM articles WHERE project_id='".$project->get_id()."'");
		if ($first_operation && $second_operation) return true;
		return false;
	}
        
        // Move project to archive:
        public static function move_project_to_archive($project_id) {
            return mysql_query("UPDATE projects SET archived='1' WHERE id='$project_id'");
        }
        
        // Move project from archive:
        public static function move_project_from_archive($project_id) {
            return mysql_query("UPDATE projects SET archived='0' WHERE id='$project_id'");
        }
	
	// Add new article request:
	public static function add_article($article) {
		$operation_status = mysql_query("INSERT INTO articles SET
			project_id='".$article->get_project_id()."',
			title='".$article->get_title()."',
			url='".$article->get_url()."',
			project_description='".$article->get_project_description()."',
			text_type='".$article->get_text_type()."',
			category='".$article->get_category()."',
			length='".$article->get_length()."',
			writers_type='".$article->get_writers_type()."',
			keywords='".$article->get_keywords()."',
			price='".$article->get_price()."',
			text_style='".$article->get_text_style()."',
			article_purpose='".$article->get_article_purpose()."',
			special_instructions='".$article->get_special_instructions()."',
			writers_list='".$article->get_writers_list()."'
		");
		if ($operation_status) return true;
		return false;
	}
	
	// Update article request:
	public static function update_article($article) {
		$operation_status = mysql_query("UPDATE articles SET
			project_id='".$article->get_project_id()."',
			title='".$article->get_title()."',
			url='".$article->get_url()."',
			project_description='".$article->get_project_description()."',
			text_type='".$article->get_text_type()."',
			category='".$article->get_category()."',
			length='".$article->get_length()."',
			writers_type='".$article->get_writers_type()."',
			keywords='".$article->get_keywords()."',
			price='".$article->get_price()."',
			text_style='".$article->get_text_style()."',
			article_purpose='".$article->get_article_purpose()."',
			special_instructions='".$article->get_special_instructions()."',
			writers_list='".$article->get_writers_list()."'
			WHERE id='".$article->get_id()."'
		");
		if ($operation_status) return true;
		return false;
	}
	
	// Update article text:
	public static function update_article_text($article_id, $article_title, $article_text) {
                $article_title = addslashes($article_title);
                $article_text = addslashes($article_text);
		$operation_status = mysql_query("UPDATE articles SET
			article_title='$article_title',
			text='$article_text'
			WHERE id='$article_id'
		");
		if ($operation_status) return true;
		return false;
	}
        
	// Update article text:
	public static function update_article_formatted_text($article_id, $article_text) {
                $article_text = addslashes($article_text);
		$operation_status = mysql_query("UPDATE articles SET
			formatted_text='$article_text'
			WHERE id='$article_id'
		");
		if ($operation_status) return true;
		return false;
	}
	
	// Update article status:
	public static function update_article_status($service_project_id, $status, $review_url = '', $article_id = 0) {
		$current_time = time();
		
		if ($review_url && $article_id) $additional_query = "review_url='$review_url', service_article_id='$article_id',";
		else $additional_query = '';
		
		$operation_status = mysql_query("UPDATE articles SET
			status='$status',
			$additional_query
			check_time='$current_time'
			WHERE service_project_id='$service_project_id'
		");
		if ($operation_status) return true;
		return false;
	}
        
        // Delete article:
        public static function delete_article($article_id) {
            $article_id = addslashes($article_id);
            $operation_status = mysql_query("DELETE FROM articles WHERE id='$article_id'");
            if ($operation_status) return true;
            return false;
	}
        
        // Get templates:
        public static function get_templates() {
            $templates_result = mysql_query("SELECT * FROM templates");
            $templates_array = array();
            while ($template = mysql_fetch_array($templates_result)) {
                    $templates_array[] = new Template($template);
            }
            return $templates_array;
        }
        
        // Get template:
        public static function get_template($template_id) {
            $template_result = mysql_query("SELECT * FROM templates WHERE id='$template_id'");
            if (mysql_num_rows($template_result)) {
                return mysql_fetch_array($template_result);
            }
            return false;
        }
        
        // Add template:
        public static function add_template($template) {
            $operation_status = mysql_query("INSERT INTO templates SET
                name='".$template->get_name()."',
                description='".$template->get_description()."',
                vars='".$template->get_vars()."'
            ");
            if ($operation_status) return true;
            return false;
        }
        
        // Update template:
        public static function update_template($template) {
            $operation_status = mysql_query("UPDATE templates SET
                name='".$template->get_name()."',
                description='".$template->get_description()."',
                vars='".$template->get_vars()."'
                WHERE id='".$template->get_id()."'
            ");
            if ($operation_status) return true;
            return false;
        }
        
	// Delete template:
	public static function delete_template($template_id) {
            $operation_status = mysql_query("DELETE FROM templates WHERE id='$template_id'");
            if ($operation_status) return true;
            return false;
	}
}

class Article {
	function __construct($article_data) {
		if (is_array($article_data)) {
			// Берем данные из массива:
			$this->database_array_to_fields($article_data);
		} elseif (is_numeric($article_data)) {
			// Заполняем из базы данных:
			$article_result = mysql_query("SELECT * FROM articles WHERE id='$article_data'");
			if (mysql_num_rows($article_result) != 0) {
				$article_array = mysql_fetch_array($article_result);
				$this->database_array_to_fields($article_array);
			}
		}
	}
	
	private function database_array_to_fields($database_array) {
		$this->id = $database_array['id'];
		$this->service_project_id = $database_array['service_project_id'];
		$this->service_article_id = $database_array['service_article_id'];
		$this->project_id = $database_array['project_id'];
		$this->title = $database_array['title'];
		$this->article_title = $database_array['article_title'];
		$this->url = $database_array['url'];
		$this->project_description = $database_array['project_description'];
		$this->text_type = $database_array['text_type'];
		$this->category = $database_array['category'];
		$this->length = $database_array['length'];
		$this->writers_type = $database_array['writers_type'];
		$this->keywords = $database_array['keywords'];
		$this->price = $database_array['price'];
		$this->text_style = $database_array['text_style'];
		$this->article_purpose = $database_array['article_purpose'];
		$this->special_instructions = $database_array['special_instructions'];
		$this->review_url = $database_array['review_url'];
		$this->writers_list = $database_array['writers_list'];
		$this->text = $database_array['text'];
                $this->formatted_text = $database_array['formatted_text'];
		$this->status = $database_array['status'];
		$this->check_time = $database_array['check_time'];
	}

	// Getters:
	public function get_id() {
		return $this->id;
	}
	public function get_service_project_id() {
		return $this->service_project_id;
	}
	public function get_service_article_id() {
		return $this->service_article_id;
	}
	public function get_project_id() {
		return $this->project_id;
	}
	public function get_title() {
		return $this->title;
	}
	public function get_article_title() {
		return $this->article_title;
	}
	public function get_url() {
		return $this->url;
	}
	public function get_project_description() {
		return $this->project_description;
	}
	public function get_text_type() {
		return $this->text_type;
	}
	public function get_category() {
		return $this->category;
	}
	public function get_length() {
		return $this->length;
	}
	public function get_writers_type() {
		return $this->writers_type;
	}
	public function get_keywords() {
		return $this->keywords;
	}
	public function get_price() {
		return $this->price;
	}
	public function get_text_style() {
		return $this->text_style;
	}
	public function get_article_purpose() {
		return $this->article_purpose;
	}
	public function get_special_instructions() {
		return $this->special_instructions;
	}
	public function get_text() {
		return $this->text;
	}
	public function get_formatted_text() {
		return $this->formatted_text;
	}
	public function get_review_url() {
		return $this->review_url;
	}
	public function get_writers_list() {
		return $this->writers_list;
	}
	public function get_status() {
		return $this->status;
	}
	public function get_human_status() {
		switch ($this->status) {
			case "open":
				return "<span class='label label-default'>Ожидает исполнителя</span>";
				break;
			case "in_use":
				return "<span class='label label-info'>Статья в процессе написания</span>";
				break;
			case "available":
				return "<span class='label label-warning'>Статья ждёт проверки</span>";
				break;
			case "closed":
				return "<span class='label label-success'>Готова к использованию</span>";
				break;
		}
		
	}
	public function get_check_time() {
		return $this->check_time;
	}
	
	private $id;
	private $service_project_id;
	private $service_article_id;
	private $project_id;
	private $title;
	private $article_title;
	private $url;
	private $project_description;
	private $text_type;
	private $category;
	private $length;
	private $writers_type;
	private $keywords;
	private $price;
	private $text_style;
	private $article_purpose;
	private $special_instructions;
	private $text;
        private $formatted_text;
	private $review_url;
	private $writers_list;
	private $status;
	private $check_time;
}

class Project {
	function __construct($project_data) {
		if (is_array($project_data)) {
			// Заполняем из массива:
			$this->database_array_to_fields($project_data);
		} elseif (is_numeric($project_data)) {
			// Заполняем из базы данных:
			$project_result = mysql_query("SELECT * FROM projects WHERE id='$project_data'");
			if (mysql_num_rows($project_result) != 0) {
				$project_array = mysql_fetch_array($project_result);
				$this->database_array_to_fields($project_array);
			}
		} else die("Project create error!");
	}
	
	public function get_articles() {
		$articles_result = mysql_query("SELECT * FROM articles WHERE project_id='".$this->get_id()."'");
		$result_array = array();
		while ($article_array = mysql_fetch_array($articles_result)) {
			$result_array[] = new Article($article_array);
		}
		return $result_array;
	}
	
	public function get_articles_count() {
		return mysql_result(mysql_query("SELECT COUNT(*) FROM articles WHERE project_id='".$this->get_id()."'"),0);
	}
        
	public function get_finished_articles_count() {
		return mysql_result(mysql_query("SELECT COUNT(*) FROM articles WHERE project_id='".$this->get_id()."' AND status='closed'"),0);
	}

	// Getters:
	public function get_id() {
		return $this->id;
	}
	public function get_name() {
		return $this->name;
	}
	public function get_url() {
		return $this->url;
	}
	public function get_archived() {
		return $this->archived;
	}
	
	private function database_array_to_fields($database_array) {
		$this->id = $database_array['id'];
		$this->name = $database_array['name'];
		$this->url = $database_array['url'];
                $this->archived = $database_array['archived'];
	}
	
	private $id;
	private $name;
	private $url;
        private $archived;
}

class Category {

	public function get_name() {
		return $this->name;
	}

	function __construct($category_service_id) {
		$category_result = mysql_query("SELECT * FROM categories WHERE service_id='$category_service_id'");
		if (mysql_num_rows($category_result)) {
			$category_array = mysql_fetch_array($category_result);
			$this->id = $category_array['id'];
			$this->service_id = $category_array['service_id'];
			$this->name = $category_array['name'];
		}
	}
	
	private $id;
	private $service_id;
	private $name;
}

class Template {
    function __construct($data) {
        if (is_array($data)) {
            $this->array_to_object_fields($data);
        }
        if (is_numeric($data)) {
            $this->load_by_id($data);
        }
        
    }
    
    public function get_id() {
        return $this->id;
    }
    
    public function get_name() {
        return $this->name;
    }
    
    public function get_description() {
        return $this->description;
    }
    
    public function get_description_array() {
        $description_array = unserialize($this->description);
        foreach ($description_array as $name => $value) {
            $new_description_array[$name] = base64_decode($value);
        }
        return $new_description_array;
    }
    
    public function get_vars() {
        return $this->vars;
    }
    
    private function load_by_id($template_id) {
        $this->array_to_object_fields(SmartDB::get_template($template_id));
    }
    
    private function array_to_object_fields($data_array) {
        $this->id = $data_array['id'];
        $this->name = $data_array['name'];
        $this->description = stripslashes($data_array['description']);
        $this->vars = $data_array['vars'];
    }
    
    private $id;
    private $name;
    private $description;
    private $vars;
}

class HTTPClient {
	/*
		HTTPClient (cURL) Settings:
		*. CURLOPT_URL:              The URL to fetch.
		*. CURLOPT_USERAGENT:        The contents of the "User-Agent" header to be used in a HTTP request.
		*. CURLOPT_HEADER:           TRUE to include the header in the output.
		*. CURLOPT_TIMEOUT:          The maximum number of seconds to allow cURL functions to execute.
		*. CURLOPT_CONNECTTIMEOUT:   The number of seconds to wait while trying to connect.
		*. CURLOPT_POST:             TRUE to do a regular HTTP POST.
		*. CURLOPT_POSTFIELDS:       The full data to post in a HTTP "POST" operation.
		*. CURLOPT_COOKIEFILE:       The name of the file containing the cookie data.
		*. CURLOPT_RETURNTRANSFER:   TRUE to return the transfer as a string instead of outputting it out directly.
		*. CURLOPT_FOLLOWLOCATION:   TRUE to follow any "Location: " header that the server sends as part of the HTTP header.
		
		Docs on PHP.net:
		http://www.php.net/manual/ru/function.curl-setopt.php
	*/
	
	// Default parameters:
	const DEFAULT_TIMEOUT = 120;
	const DEFAULT_CONNCECT_TIMEOUT = 120;
	const DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0";

	public function __construct($url = NULL) {

		// Initialization cURL:
		$this->curl = curl_init($url);
		
		// Default cURL settings:
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->curl, CURLOPT_HEADER, FALSE);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, self::DEFAULT_TIMEOUT);
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_CONNCECT_TIMEOUT);
		curl_setopt($this->curl, CURLOPT_USERAGENT, self::DEFAULT_USER_AGENT);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, FALSE);
#		curl_setopt($this->curl, CURLOPT_PROXY, "127.0.0.1:8080");
		curl_setopt($this->curl, CURLOPT_COOKIEFILE, '');
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	}

	public function do_request() {
		$result = curl_exec($this->curl);
		if (!$result) {
			$this->curl_errors[] = curl_error($this->curl)." (".curl_errno($this->curl).").";
			return false;
		}
		return $result;
	}

	public function set_url($url) {
		$this->url = $url;
		curl_setopt($this->curl, CURLOPT_URL, $this->url); 
	}
	
	public function set_user_agent($user_agent) {
		$this->user_agent = $user_agent;
		curl_setopt($this->curl, CURLOPT_USERAGENT, $this->user_agent);
	}
	
	public function set_post_method($post_data) {
		curl_setopt($this->curl, CURLOPT_POST, TRUE);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_data);
	}
	
	public function set_get_method() {
		curl_setopt($this->curl, CURLOPT_HTTPGET, TRUE);
	}
	
	public function close_client() {
		curl_close($this->curl);
	}
	
	private $url;
	private $user_agent;
	private $curl;
	private $curl_errors = array();
}

class SelectOptions {
	function __construct($options) {
		$this->options = $options;
	}
	public function get_html() {
		$html = '';
		foreach($this->options as $value => $text) {
			$selected = ($value == $this->current_value) ? " selected='selected'" : '';
			$html .= "<option$selected value='$value'>$text</option>";
		}
		return $html;
	}
	public function set_current_value($current_value) {
		$this->current_value = $current_value;
	}
	private $options;
	private $current_value;
}

?>