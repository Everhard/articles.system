<?php
class Controller {
	public static function start() {
		// Make all actions:
		self::make_actions();
		// Process parameters:
		self::process_url_parameters();
		// Include header:
		require_once("modules/includes/header.php");
		// Include module:
		require_once("modules/".self::get_first_parameter().".php");
		// Include footer:
		require_once("modules/includes/footer.php");
	}
	
	public static function show_message() {
		if (isset(self::$client_message)) {
			$type = self::$client_message['type'];
			$message = self::$client_message['message'];
			echo "<div class='alert alert-$type'>$message</div>\n";
		}
	}
	
	public static function get_first_parameter() {
		return self::$url_parameters['module'];
	}
	
	public static function get_second_parameter() {
		return self::$url_parameters['2nd_parameter'];
	}
	
	private static function process_url_parameters() {
		// Default module:
		$url_parameters['module'] = "projects";
		$url_parameters['2nd_parameter'] = '';
		// Get module from URL:
		if (isset($_GET['module'])) {
			$url_parts = explode("/", $_GET['module']);
			$url_parameters['module'] = addslashes(trim($url_parts[0]));
			if (count($url_parts) == 2) $url_parameters['2nd_parameter'] = addslashes(trim($url_parts[1]));
		}
		
		// Module filter:
		if (!in_array($url_parameters['module'], self::$allow_modules)) $url_parameters['module'] = "notfound";
		
		self::$url_parameters = $url_parameters;
		
		return true;
	}
	
	private static function make_actions() {
		if (isset($_POST['action-module']) && isset($_POST['action-method'])) {
			// Parameters:
			$action_module = addslashes(trim($_POST['action-module']));
			$action_method = addslashes(trim($_POST['action-method']));
			switch($action_module) {
				// Process projects request:
				case "projects":
					if ($action_method == "add-project") {
						if (isset($_POST['project-name']) && isset($_POST['project-url'])) {
							$project = new Project(array(
								"id" => 'none',
								"name" => addslashes(trim($_POST['project-name'])),
								"url" => addslashes(trim($_POST['project-url']))
							));
							if (SmartDB::add_project($project))
								self::put_message("success", "Проект был успешно добавлен!");
							else self::put_message("danger", "Ошибка добавления проекта!");
						}
					}
					if ($action_method == "edit-project") {
						if (isset($_POST['project-name']) && isset($_POST['project-url']) && isset($_POST['project-id'])) {
							$project = new Project(array(
								"id" => addslashes(trim($_POST['project-id'])),
								"name" => addslashes(trim($_POST['project-name'])),
								"url" => addslashes(trim($_POST['project-url']))
							));
							if (SmartDB::update_project($project))
								self::put_message("success", "Проект был успешно обновлен!");
							else self::put_message("danger", "Ошибка обновления проекта!");
						}
					}
					if ($action_method == "delete-project") {
						if (isset($_POST['project-id']) && is_numeric($_POST['project-id'])) {
							$project = new Project(array(
								"id" => addslashes(trim($_POST['project-id'])),
								"name" => 'none',
								"url" => 'none'
							));
							if (SmartDB::delete_project($project))
								self::put_message("success", "Проект был успешно удалён!");
							else self::put_message("danger", "Ошибка удаления проекта!");
						}
					}
                                        if ($action_method == "move-to-archive") {
                                            $project_id = addslashes(trim($_POST['project-id']));
                                            if ( SmartDB::move_project_to_archive($project_id))
                                                self::put_message("success", "Проект был успешно перемещён в архив!");
                                            else self::put_message("danger", "Ошибка перемещения проекта в архив!");
                                        }
                                        if ($action_method == "move-from-archive") {
                                            $project_id = addslashes(trim($_POST['project-id']));
                                            if ( SmartDB::move_project_from_archive($project_id))
                                                self::put_message("success", "Проект был успешно перемещён из архива!");
                                            else self::put_message("danger", "Ошибка перемещения проекта из архива!");
                                        }
					break;

				// Process articles requests:
				case "articles":
					if ($action_method == "add-request-article") {
						$article = new Article(array(
							"id" => 'none',
							"project_id" => addslashes(trim($_POST['project-id'])),
                                                        "service_project_id" => '',
                                                        "service_article_id" => '',
							"title" => addslashes(trim($_POST['title'])),
                                                        "article_title" => '',
							"url" => addslashes(trim($_POST['url'])),
							"project_description" => addslashes(trim($_POST['project-description'])),
							"text_type" => addslashes(trim($_POST['text-type'])),
							"category" => addslashes(trim($_POST['category'])),
							"length" => addslashes(trim($_POST['length'])),
							"writers_type" => addslashes(trim($_POST['writers-type'])),
							"keywords" => addslashes(trim($_POST['keywords'])),
							"price" => addslashes(trim($_POST['price'])),
							"text_style" => addslashes(trim($_POST['text-style'])),
							"article_purpose" => addslashes(trim($_POST['article-purpose'])),
							"special_instructions" => addslashes(trim($_POST['special-instructions'])),
							"writers_list" => addslashes(trim($_POST['writers-list'])),
							"text" => "none",
                                                        "review_url" => '',
                                                        "status" => '',
                                                        "check_time" => ''
						));
						if (SmartDB::add_article($article))
							self::put_message("success", "Запрос на написание статьи был успешно создан!");
						else self::put_message("danger", "Ошибка создания запроса на написание статьи!");
					}
					if ($action_method == "edit-article-request") {
						$article = new Article(array(
							"id" => addslashes(trim($_POST['article-id'])),
							"project_id" => addslashes(trim($_POST['project-id'])),
                                                        "service_project_id" => '',
                                                        "service_article_id" => '',
							"title" => addslashes(trim($_POST['title'])),
                                                        "article_title" => '',
							"url" => addslashes(trim($_POST['url'])),
							"project_description" => addslashes(trim($_POST['project-description'])),
							"text_type" => addslashes(trim($_POST['text-type'])),
							"category" => addslashes(trim($_POST['category'])),
							"length" => addslashes(trim($_POST['length'])),
							"writers_type" => addslashes(trim($_POST['writers-type'])),
							"keywords" => addslashes(trim($_POST['keywords'])),
							"price" => addslashes(trim($_POST['price'])),
							"text_style" => addslashes(trim($_POST['text-style'])),
							"article_purpose" => addslashes(trim($_POST['article-purpose'])),
							"special_instructions" => addslashes(trim($_POST['special-instructions'])),
							"writers_list" => addslashes(trim($_POST['writers-list'])),
							"text" => "none",
                                                        "review_url" => '',
                                                        "status" => '',
                                                        "check_time" => ''
						));
						if (SmartDB::update_article($article))
							self::put_message("success", "Запрос на написание статьи был успешно изменён!");
						else self::put_message("danger", "Ошибка изменения запроса на написание статьи!");
					}
					if ($action_method == "push-request-article") {
						$article_id = addslashes(trim($_POST['article-id']));
						$article = new Article($article_id);
						
						if (ArticleService::push_article_request($article))
							self::put_message("success", "Запрос на написание статьи был успешно создан!");
						else self::put_message("danger", "Ошибка создания запроса на написание статьи!");
					}
					if ($action_method == "check-article-status") {
						self::put_message("danger", "Ошибка проверки статуса статьи!");
						$service_project_id = addslashes(trim($_POST['service-project-id']));
						$article_status = ArticleService::get_project_status($service_project_id);
						if ($article_status) {
						
							if (in_array($article_status['status'], array("open", "in_use"))) {
								$operation = SmartDB::update_article_status($service_project_id, $article_status['status']);
							}
								
							if ($article_status['status'] == "available") {
								$operation = SmartDB::update_article_status($service_project_id, $article_status['status'], $article_status['review_url'], $article_status['article_id']);
							}

							if ($operation) {
								self::put_message("success", "Статус статьи был успешно проверен!");
							}
						}
					}
					if ($action_method == "check-articles-status") {
						self::put_message("danger", "Ошибка проверки статуса статей!");
						$project_id = addslashes(trim($_POST['project-id']));
						$project = new Project($project_id);
						$articles = $project->get_articles();
						
						foreach ($articles as $article) {
							if ($article->get_status() != 'closed') {
								
								$article_status = ArticleService::get_project_status($article->get_service_project_id());
								
								if ($article_status) {
								
									if (in_array($article_status['status'], array("open", "in_use"))) {
										$operation = SmartDB::update_article_status($article->get_service_project_id(), $article_status['status']);
									}
										
									if ($article_status['status'] == "available") {
										$operation = SmartDB::update_article_status($article->get_service_project_id(), $article_status['status'], $article_status['review_url'], $article_status['article_id']);
									}
								}
							}
						}
						
						if (isset($operation) && $operation) {
							self::put_message("success", "Статус статей был успешно проверен!");
						}
					}
					if ($action_method == "approve-article") {
						self::put_message("danger", "Ошибка одобрения статьи!");
						$article_id = addslashes(trim($_POST['article-id']));
						$article = new Article($article_id);
						// Comment:
						$comment = "Excellent article! Thank you!";
						
						$operation_status = ArticleService::approve_article($article->get_service_project_id(), $article->get_service_article_id(), $comment);
						if ($operation_status) {
							if (SmartDB::update_article_status($article->get_service_project_id(), "closed")) {
								$article_array = ArticleService::download_article($article->get_service_project_id());
								if ($article_array) {
									if (SmartDB::update_article_text($article->get_id(), $article_array['title'], $article_array['text']))
										self::put_message("success", "Статья была успешно одобрена!");
								}
							}
						}
					}
                                        if ($action_method == "update-formatted-text") {
                                            self::put_message("danger", "Ошибка обновления текста!");
                                            $article_id = addslashes(trim($_POST['article-id']));
                                            $formatted_text = addslashes(trim($_POST['formatted-text']));
                                            
                                            if (SmartDB::update_article_formatted_text($article_id, $formatted_text)) {
                                                self::put_message("success", "Текст был успешно обновлён!");
                                            }
                                        }
                                        if ($action_method == "delete-article") {
                                            self::put_message("danger", "Ошибка удаления статьи!");
                                            $article_id = addslashes(trim($_POST['article-id']));
                                            
                                            if (SmartDB::delete_article($article_id)) {
                                                self::put_message("success", "Статья была успешно удалена!");
                                            }
                                        }
					break;
					
				// Process categories requests:
				case "categories":
					if ($action_method == "reload-categories") {
						if (SmartDB::update_categories(ArticleService::get_categories()))
							self::put_message("success", "Категории были успешно обновлены!");
						else self::put_message("danger", "Произошла ошибка обновления категорий!");
					}
					break;
                                // Process templates requests:
                                case "template":
                                    if ($action_method == "add-template") {
                                        
                                        $vars_array = array();
                                        if (!empty($_POST['var-name'])) {
                                            foreach ($_POST['var-name'] as $key => $varname) {
                                                if ($varname) $vars_array[$varname] = $_POST['var-description'][$key];
                                            }
                                        } 
                                        
                                        $name = addslashes(trim($_POST['name']));
                                        
                                        $template_description = serialize(array(
                                            "title" => base64_encode(trim($_POST['title'])),
                                            "url" => base64_encode(trim($_POST['url'])),
                                            "project-description" => base64_encode(trim($_POST['project-description'])),
                                            "text-type" => base64_encode(trim($_POST['text-type'])),
                                            "category" => base64_encode(trim($_POST['category'])),
                                            "length" => base64_encode(trim($_POST['length'])),
                                            "writers-type" => base64_encode(trim($_POST['writers-type'])),
                                            "keywords" => base64_encode(trim($_POST['keywords'])),
                                            "price" => base64_encode(trim($_POST['price'])),
                                            "text-style" => base64_encode(trim($_POST['text-style'])),
                                            "article-purpose" => base64_encode(trim($_POST['article-purpose'])),
                                            "special-instructions" => base64_encode(trim($_POST['special-instructions'])),
                                            "writers-list" => base64_encode(trim($_POST['writers-list']))
                                        ));
                                        
                                        $template = new Template(array(
                                            "id" => 'none',
                                            "name" => $name,
                                            "description" => $template_description,
                                            "vars" => addslashes(serialize($vars_array))
                                        ));
                                        
                                        self::put_message("danger", "Произошла ошибка при добавлении шаблона!");
                                        if (SmartDB::add_template($template)) {
                                            self::put_message("success", "Шаблон был успешно добавлен!");
                                        }
                                        
                                    }
                                    if ($action_method == "edit-template") {
                                        
                                        $id = addslashes(trim($_POST['id']));
                                        $name = addslashes(trim($_POST['name']));
                                        
                                        $vars_array = array();
                                        if (!empty($_POST['var-name'])) {
                                            foreach ($_POST['var-name'] as $key => $varname) {
                                                if ($varname) $vars_array[$varname] = $_POST['var-description'][$key];
                                            }
                                        }
                                        
                                        $template_description = serialize(array(
                                            "title" => base64_encode(trim($_POST['title'])),
                                            "url" => base64_encode(trim($_POST['url'])),
                                            "project-description" => base64_encode(trim($_POST['project-description'])),
                                            "text-type" => base64_encode(trim($_POST['text-type'])),
                                            "category" => base64_encode(trim($_POST['category'])),
                                            "length" => base64_encode(trim($_POST['length'])),
                                            "writers-type" => base64_encode(trim($_POST['writers-type'])),
                                            "keywords" => base64_encode(trim($_POST['keywords'])),
                                            "price" => base64_encode(trim($_POST['price'])),
                                            "text-style" => base64_encode(trim($_POST['text-style'])),
                                            "article-purpose" => base64_encode(trim($_POST['article-purpose'])),
                                            "special-instructions" => base64_encode(trim($_POST['special-instructions'])),
                                            "writers-list" => base64_encode(trim($_POST['writers-list']))
                                        ));
                                        
                                        $template = new Template(array(
                                            "id" => $id,
                                            "name" => $name,
                                            "description" => $template_description,
                                            "vars" => addslashes(serialize($vars_array))
                                        ));
                                        
                                        self::put_message("danger", "Произошла ошибка при обновлении шаблона!");
                                        if (SmartDB::update_template($template)) {
                                            self::put_message("success", "Шаблон был успешно обновлён!");
                                        }
                                        
                                    }
                                   if ($action_method == "delete-template") {
                                        
                                        $id = addslashes(trim($_POST['id']));
                                        
                                        self::put_message("danger", "Произошла ошибка при удалении шаблона!");
                                        if (SmartDB::delete_template($id)) {
                                            self::put_message("success", "Шаблон был успешно удалён!");
                                        }
                                        
                                    }
                                    
                                        break;
			}
		}
	}
	
	private static function put_message($type, $message) {
		self::$client_message['type'] = $type;
		self::$client_message['message'] = $message;
	}
	
	// Allow modules:
	private static $allow_modules = array(
		"projects",
                "archive",
		"project",
		"request-article",
		"categories",
		"view-article",
		"edit-article",
                "templates",
                "add-template",
                "edit-template",
                "format-article"
	);
	
	private static $url_parameters;
	
	// Message for client:
	private static $client_message;
}
?>