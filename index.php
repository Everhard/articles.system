<?php
// Configuration:
require_once("config.php");
// Core:
require_once("lib/core.php");
// Controller:
require_once("lib/controller.php");
// Start:
Controller::start();
?>