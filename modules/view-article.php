<div class="container">

	<?php Controller::show_message(); ?>
	<?php
		$article = new Article(Controller::get_second_parameter());
		$project = new Project($article->get_project_id());
		$category = new Category($article->get_category());
	?>

	<div class="panel panel-default">
		<div class="panel-heading">Проект <strong><a href="/project/<?php echo $project->get_id(); ?>"><?php echo $project->get_name(); ?></a></strong> / Статья <strong><?php echo $article->get_title(); ?></strong></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-6">
					<div id="article" class="text-justify well">
						<h3>
							<?php echo $article->get_title(); ?><br />
							<small><?php echo $article->get_article_title(); ?></small>
						</h3>
						<?php
						if ($article->get_status() == "available") echo "<a href='".$article->get_review_url()."'><img style='width: 100%;' src='".$article->get_review_url()."' alt='' /></a>";
						else echo $article->get_text() ? $article->get_text() : "Статья ещё не готова.";
						?>
					</div>
				</div>
				<div class="col-lg-6">
					<table class="table table-bordered">
						<thead>
							<th>Параметр</th>
							<th>Значение</th>
						</thead>
						<tbody>
							<tr>
								<td>URL</td>
								<td><?php echo $article->get_url() ? $article->get_url() : "Нет"; ?></td>
							</tr>
							<tr>
								<td>Тип текста</td>
								<td><?php echo $article->get_text_type(); ?></td>
							</tr>
							<tr>
								<td>Категория</td>
								<td><?php echo $category->get_name(); ?></td>
							</tr>
							<tr>
								<td>Объем</td>
								<td><?php echo $article->get_length(); ?></td>
							</tr>
							<tr>
								<td>Какие исполнители?</td>
								<td><?php echo $article->get_writers_type(); ?></td>
							</tr>
							<tr>
								<td>Стиль написания</td>
								<td><?php echo $article->get_text_style(); ?></td>
							</tr>
							<?php if ($article->get_status()) { ?>
							<tr>
								<td>Статус</td>
								<td><?php echo $article->get_human_status(); ?></td>
							</tr>
							<tr>
								<td>Время последней проверки:</td>
								<td><?php echo date("H:i (d.m.Y)", $article->get_check_time()); ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
                                        <?php echo $article->get_formatted_text() ? "<p><a class='btn btn-primary' href='/format-article/".$article->get_id()."'>Открыть форматированный текст</a></p>" : ""; ?>
					<h3>Описание проекта:</h3>
					<p><?php echo $article->get_project_description(); ?></p>
					<h3>Цель статьи:</h3>
					<p><?php echo $article->get_article_purpose(); ?></p>
					<h3>Специальные инструкции:</h3>
					<p><?php echo $article->get_special_instructions(); ?></p>
					<h3>Ключевые слова:</h3>
					<p><?php echo $article->get_keywords(); ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<p>
						<?php if (!$article->get_status()) { ?> <a id="push-article-request-button" class='btn btn-primary' href="#">Отправить на написание</a><?php } ?>
						<?php if (in_array($article->get_status(), array("open", "in_use"))) { ?> <a id="check-article-status-button" class='btn btn-primary' href="#">Проверить статус</a><?php } ?>
						<?php if ($article->get_status() == "available") { ?> <a id="approve-article-button" class='btn btn-primary' href="#">Одобрить статью</a><?php } ?>
						<a class='btn btn-default' href="/edit-article/<?php echo $article->get_id(); ?>">Редактировать</a>
                                                <?php if ($article->get_status() == "closed") { ?><a class="btn btn-default" href="/format-article/<?php echo $article->get_id(); ?>">Отформатировать</a><?php } ?>
					</p>
				</div>
				<div class="col-lg-6">
					<p class="text-right">
						<a class='btn btn-default' href="/project/<?php echo $project->get_id(); ?>">Закрыть</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if (!$article->get_status()) { ?>
<form id="push-article-request" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="push-request-article" />
	<input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
</form>
<?php } ?>

<?php if (in_array($article->get_status(), array("open", "in_use"))) { ?>
<form id="check-article-status" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="check-article-status" />
	<input type="hidden" name="service-project-id" value="<?php echo $article->get_service_project_id(); ?>" />
</form>
<?php } ?>

<?php if ($article->get_status() == "available") { ?>
<form id="approve-article" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="approve-article" />
	<input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
</form>
<?php } ?>