<div class="container">

	<?php Controller::show_message(); ?>

	<div class="panel panel-default">
		<div class="panel-heading">Проекты</div>
		<div class="panel-body">
			<button class='btn btn-default' data-toggle="modal" data-target="#add-project">Добавить проект</button>
		</div>
		<table class="table table-striped" id="prjects-list-table">
			<thead>
				<th>Проект</th>
				<th class="hidden-xs">Количество статей</th>
				<th>Управление</th>
			</thead>
			<tbody>
<?php
$projects = SmartDB::get_projects();
if (count($projects) > 0) {
	foreach ($projects as $project) {
		echo "<tr>
					<td>
						<p><a class='project-name-tag' href='/project/".$project->get_id()."'>".$project->get_name()."</a></p>
						<p><small class='project-url-tag text-muted'>".$project->get_url()."</small></p>
					</td>
					<td>".$project->get_finished_articles_count()." / ".$project->get_articles_count()."</td>
					<td>
						<button title='Редактировать проект' class='btn btn-default btn-xs edit-project-button' data-id='".$project->get_id()."'><span class='glyphicon glyphicon-edit'></span></button>
						<button title='Удалить проект' class='btn btn-default btn-xs delete-project-button' data-id='".$project->get_id()."'><span class='glyphicon glyphicon-remove'></span></button>
					</td>
				</tr>";
	}
} else echo "<tr><td colspan='3' class='text-center'>Нет проектов.</td></tr>";
?>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="add-project">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Новый проект</h4>
	  </div>
	  <div class="modal-body">
		<form role="form" method="post" id="add-project-form">
		  <div class="form-group">
			<label>Новый проект</label>
			<input type="text" name="project-name" class="form-control" placeholder="Название нового проекта">
		  </div>
		  <div class="form-group">
			<label>URL ресурса</label>
			<input type="text" name="project-url" class="form-control" placeholder="Например, http://mysite.com.ua">
		  </div>
		  <input type="hidden" name="action-module" value="projects" />
		  <input type="hidden" name="action-method" value="add-project" />
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
		<button type="button" class="btn btn-primary" id="add-project-button">Добавить</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="edit-project">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Редактирование проекта</h4>
	  </div>
	  <div class="modal-body">
		<form role="form" method="post" id="edit-project-form">
		  <div class="form-group">
			<label>Название проекта</label>
			<input type="text" name="project-name" class="form-control" placeholder="Новое название проекта">
		  </div>
		  <div class="form-group">
			<label>URL ресурса</label>
			<input type="text" name="project-url" class="form-control" placeholder="Например, http://mysite.com.ua">
		  </div>
		  <input type="hidden" name="project-id" value="" />
		  <input type="hidden" name="action-module" value="projects" />
		  <input type="hidden" name="action-method" value="edit-project" />
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
		<button type="button" class="btn btn-primary" id="edit-project-button">Обновить</button>	
	  </div>
	</div>
  </div>
</div>

<form id="delete-project" method="post">
	<input type="hidden" name="action-module" value="projects" />
	<input type="hidden" name="action-method" value="delete-project" />
	<input type="hidden" name="project-id" value="" />
</form>