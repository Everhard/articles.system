<div class="container">

	<?php Controller::show_message(); ?>

	<div class="panel panel-default">
		<div class="panel-heading">Категории статей</div>
		<div class="panel-body">
			<a class='btn btn-default' id="reload-categories-button" href="#">Обновить категории</a>
		</div>
		<table class="table table-striped table-condensed" id="articles-list-table">
			<thead>
				<th>ID</th>
				<th>Название</th>
			</thead>
			<tbody>
<?php
$categories = SmartDB::get_categories();
if (count($categories) > 0) {
	foreach ($categories as $category) {
		echo "<tr>
					<td>$category[service_id]</td>
					<td>$category[name]</td>
				</tr>";
	}
} else echo "<tr><td colspan='2' class='text-center'>Нет категорий</td></tr>";
?>
			</tbody>
		</table>
	</div>
</div>

<form id="reload-categories" method="post">
	<input type="hidden" name="action-module" value="categories" />
	<input type="hidden" name="action-method" value="reload-categories" />
</form>