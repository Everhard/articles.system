<div class="container">

	<?php Controller::show_message(); ?>
	<?php $project = new Project(Controller::get_second_parameter()); ?>

    <div class="row">
        <div class="col-lg-9">
            <div class="panel panel-default">
                    <div class="panel-heading">Заказ статьи для проекта <strong><?php echo $project->get_name(); ?></strong></div>
                    <div class="panel-body">
                            <form class="form-horizontal" action="/project/<?php echo $project->get_id(); ?>" role="form" method="post">
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Заголовок статьи</label>
                                    <div class="col-sm-10">
                                      <input name="title" type="text" class="form-control">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">URL</label>
                                    <div class="col-sm-10">
                                      <input name="url" type="text" class="form-control">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Тип текста</label>
                                    <div class="col-sm-10">
                                      <select name="text-type" class="form-control">
                                              <option value="COPYRIGHT">Копирайтинг</option>
                                              <option value="REWRITE">Рерайтинг</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Описание проекта</label>
                                    <div class="col-sm-10">
                                      <input name="project-description" type="text" class="form-control" placeholder="Например, требуется 5 статей на тему микрокредитования">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Категория</label>
                                    <div class="col-sm-10">
                                      <select name="category" class="form-control">
                                              <?php
                                              $categories = SmartDB::get_categories();
                                              foreach ($categories as $category) {
                                                    echo "<option value='$category[service_id]'>$category[name]</option>";
                                              }
                                              ?>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Объем (количество слов)</label>
                                    <div class="col-sm-10">
                                      <select name="length" class="form-control">
                                              <option>150</option>
                                              <option>300</option>
                                              <option>400</option>
                                              <option>500</option>
                                              <option>700</option>
                                              <option>1000</option>
                                              <option>2000</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Исполнители</label>
                                    <div class="col-sm-10">
                                      <select name="writers-type" class="form-control">
                                              <option value="ALL">Все</option>
                                              <option value="PREMIUM">Премиум (от 4.1 до 5 звёзд)</option>
                                              <option value="ELITE">Элитные (от 4.6 до 5 звёзд)</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Ключевые слова</label>
                                    <div class="col-sm-10">
                                       <textarea name="keywords" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Цена</label>
                                    <div class="col-sm-10">
                                       <input name="price" type="text" class="form-control" placeholder="">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Стиль написания</label>
                                    <div class="col-sm-10">
                                      <select name="text-style" class="form-control">
                                              <option value="FRIENDLY">Дружественный</option>
                                              <option value="PROFESSIONAL">Профессиональный</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Цель статьи</label>
                                    <div class="col-sm-10">
                                       <textarea name="article-purpose" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Специальные инструкции</label>
                                    <div class="col-sm-10">
                                       <textarea name="special-instructions" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Только для указанных исполнителей</label>
                                    <div class="col-sm-10">
                                       <textarea name="writers-list" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <input type="hidden" name="action-module" value="articles" />
                                      <input type="hidden" name="action-method" value="add-request-article" />
                                      <input type="hidden" name="project-id" value="<?php echo $project->get_id(); ?>" />
                                      <button type="submit" class="btn btn-default">Сохранить</button>
                                    </div>
                              </div>
                            </form>
                    </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">Шаблоны</div>
                <div class="panel-body">
                    <?php
                        $templates = SmartDB::get_templates();
                        foreach ($templates as $template) {
                            echo "<div class='well well-sm'>
                                <h5><a class='open-template' href='#'>".$template->get_name()."</a></h5>\n<div style='display: none;'>";
                            if ($variables_array = unserialize($template->get_vars())) {
                                foreach ($variables_array as $var_name => $var_description) {
                                    echo "<div class='form-group'><input type='text' class='form-control' placeholder='$var_description' data-varname='$var_name'></div>";
                                }
                            }
                            echo "<button id='template".$template->get_id()."' class='btn btn-primary btn-sm'>Применить</button></div></div>";
                         
                        }
                    ?>
                    
                    <?php
                            echo "<script type='text/javascript'>
                                String.prototype.replaceAll=function(find, replace_to){
                                    return this.replace(new RegExp(find, 'g'), replace_to);
                                };
                                    window.onload = function() {
                                        $(document).ready(function() {";
                                        
                            foreach ($templates as $template) {
                                echo "$('#template".$template->get_id()."').click(function () {


                                    var templateDescription = ".json_encode($template->get_description_array()).";
                                    for (field in templateDescription) {
                                    
                                        var fieldValue = templateDescription[field];
                                        
                                        $(this).parent().find('input').each(function() {
                                            fieldValue = fieldValue.replace(new RegExp($(this).attr('data-varname'),'g'), $(this).val());
                                        });

                                        $('[name=' + field +']').val(fieldValue);
                                    }
                                });\n";
                            }
                            
                            echo "
                                        });
                                    }
                                  </script>";
                    ?>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>