<div class="container">

	<?php Controller::show_message(); ?>

	<div class="panel panel-default">
		<div class="panel-heading">Шаблоны</div>
		<div class="panel-body">
			<a class='btn btn-default' href="add-template">Добавить шаблон</a>
		</div>
		<table class="table table-striped" id="prjects-list-table">
			<thead>
				<th>Название</th>
				<th>Управление</th>
			</thead>
			<tbody>
<?php
$templates = SmartDB::get_templates();
if (count($templates) > 0) {
	foreach ($templates as $template) {
		echo "<tr>
					<td>
						<p><a class='project-name-tag' href='edit-template/".$template->get_id()."'>".$template->get_name()."</a></p>
					</td>
					<td>
						<button title='Удалить шаблон' class='btn btn-default btn-xs delete-template-button' data-id='".$template->get_id()."'><span class='glyphicon glyphicon-remove'></span></button>
					</td>
				</tr>";
	}
} else echo "<tr><td colspan='2' class='text-center'>Нет шаблонов.</td></tr>";
?>
			</tbody>
		</table>
	</div>
</div>

<form id="delete-template" method="post">
	<input type="hidden" name="action-module" value="template" />
	<input type="hidden" name="action-method" value="delete-template" />
	<input type="hidden" name="id" value="" />
</form>