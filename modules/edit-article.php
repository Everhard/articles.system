<div class="container">

	<?php Controller::show_message(); ?>
	<?php
		$article = new Article(Controller::get_second_parameter());
		$project = new Project($article->get_project_id());
	?>

	<div class="panel panel-default">
		<div class="panel-heading">Проект <strong><a href="/project/<?php echo $project->get_id(); ?>"><?php echo $project->get_name(); ?></a></strong> / Редактирование статьи <strong><?php echo $article->get_title(); ?></strong></div>
		<div class="panel-body">
			<form class="form-horizontal" action="/view-article/<?php echo $article->get_id(); ?>" role="form" method="post">
			  <div class="form-group">
				<label class="col-sm-2 control-label">Заголовок статьи</label>
				<div class="col-sm-10">
				  <input name="title" type="text" class="form-control" value="<?php echo $article->get_title(); ?>">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">URL</label>
				<div class="col-sm-10">
				  <input name="url" type="text" class="form-control" value="<?php echo $article->get_url(); ?>">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Тип текста</label>
				<div class="col-sm-10">
				  <select name="text-type" class="form-control">
					<?php
						$options = new SelectOptions(array(
							"COPYRIGHT" => "Копирайтинг",
							"REWRITE" => "Рерайтинг"
						));
						$options->set_current_value($article->get_text_type());
						echo $options->get_html();
					?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Описание проекта</label>
				<div class="col-sm-10">
				  <input name="project-description" type="text" class="form-control" placeholder="Например, требуется 5 статей на тему микрокредитования" value="<?php echo $article->get_project_description(); ?>">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Категория</label>
				<div class="col-sm-10">
				  <select name="category" class="form-control">
					  <?php
					  $categories = SmartDB::get_categories();
					  foreach ($categories as $category) $categories_array[$category['service_id']] = $category['name'];
					  $options = new SelectOptions($categories_array);
					  $options->set_current_value($article->get_category());
					  echo $options->get_html();
					  ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Объем (количество слов)</label>
				<div class="col-sm-10">
				  <select name="length" class="form-control">
					<?php
						$options = new SelectOptions(array(
							"150" => "150",
							"300" => "300",
							"400" => "400",
							"500" => "500",
							"700" => "700",
							"1000" => "1000",
							"2000" => "2000"
						));
						$options->set_current_value($article->get_length());
						echo $options->get_html();
					?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Исполнители</label>
				<div class="col-sm-10">
				  <select name="writers-type" class="form-control">
					<?php
						$options = new SelectOptions(array(
							"ALL" => "Все",
							"PREMIUM" => "Премиум (от 4.1 до 5 звёзд)",
							"ELITE" => "Элитные (от 4.6 до 5 звёзд)"
						));
						$options->set_current_value($article->get_writers_type());
						echo $options->get_html();
					?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Ключевые слова</label>
				<div class="col-sm-10">
				   <textarea name="keywords" class="form-control" rows="3"><?php echo $article->get_keywords(); ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Цена</label>
				<div class="col-sm-10">
				   <input name="price" type="text" class="form-control" placeholder="" value="<?php echo $article->get_price(); ?>">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Стиль написания</label>
				<div class="col-sm-10">
				  <select name="text-style" class="form-control">
					<?php
						$options = new SelectOptions(array(
							"FRIENDLY" => "Дружественный",
							"PROFESSIONAL" => "Профессиональный"
						));
						$options->set_current_value($article->get_text_style());
						echo $options->get_html();
					?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Цель статьи</label>
				<div class="col-sm-10">
				   <textarea name="article-purpose" class="form-control" rows="3"><?php echo $article->get_article_purpose(); ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Специальные инструкции</label>
				<div class="col-sm-10">
				   <textarea name="special-instructions" class="form-control" rows="3"><?php echo $article->get_special_instructions(); ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label">Только для указанных исполнителей</label>
				<div class="col-sm-10">
				   <textarea name="writers-list" class="form-control" rows="3"><?php echo $article->get_writers_list(); ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				  <input type="hidden" name="action-module" value="articles" />
				  <input type="hidden" name="action-method" value="edit-article-request" />
				  <input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
				  <input type="hidden" name="project-id" value="<?php echo $article->get_project_id(); ?>" />
				  <button type="submit" class="btn btn-default">Сохранить</button>
				</div>
			  </div>
			</form>
		</div>
	</div>
</div>