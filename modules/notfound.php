<div class="container">

	<?php Controller::show_message(); ?>

	<div class="panel panel-default">
		<div class="panel-heading">Домены</div>
		<div class="panel-body">
			<button class='btn btn-default' data-toggle="modal" data-target="#add-domain">Добавить домен</button>
			<button class='btn btn-default' id="refresh-all-domains-button">Обновить всё</button>
		</div>
		<table class="table table-striped">
			<thead>
				<th>Домен</th>
				<th class="hidden-xs">Количество обращений (24 часа)</th>
				<th>Управление</th>
			</thead>
			<tbody>
<?php
$domains = SmartDB::get_all_domains();
foreach ($domains as $domain) {
	$domain_obj = new Domain($domain[domain]);
	if ($domain_obj->is_exists()) $domain_requests = $domain_obj->get_requests_for_period_hours(24);
	echo "<tr>
			<td><a href='details/$domain[domain]'>$domain[domain]</a></td>
			<td>$domain_requests</td>
			<td><button title='Удалить домен' class='btn btn-default btn-xs delete-domain-button' data-id='$domain[id]'><span class='glyphicon glyphicon-remove'></span></button></td>
		</tr>";
}
?>
			</tbody>
		</table>
	</div>

</div>

<div class="modal fade" id="add-domain">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Новый домен</h4>
	  </div>
	  <div class="modal-body">
		<form role="form" method="post" id="add-domain-form">
		  <div class="form-group">
			<label for="new-domain">Новый домен</label>
			<input type="text" name="domain-name" class="form-control" id="new-domain" placeholder="Название новой категории">
		  </div>
		  <input type="hidden" name="action-module" value="domains" />
		  <input type="hidden" name="action-method" value="add-domain" />
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
		<button type="button" class="btn btn-primary" id="add-domain-button">Добавить</button>
	  </div>
	</div>
  </div>
</div>

<form id="delete-domain" method="post">
	<input type="hidden" name="action-module" value="domains" />
	<input type="hidden" name="action-method" value="delete-domain" />
	<input type="hidden" name="domain-id" value="" />
</form>

<form id="refresh-all-domains" method="post">
	<input type="hidden" name="action-module" value="domains" />
	<input type="hidden" name="action-method" value="get-all-domains-statistics" />
</form>