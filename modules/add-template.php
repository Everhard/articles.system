<div class="container">

	<?php Controller::show_message(); ?>

    <form class="row" action="templates" role="form" method="post">
        <div class="col-lg-9">
            <div class="panel panel-default">
                    <div class="panel-heading">Добавление шаблона</div>
                    <div class="panel-body">
                            <div class="form-horizontal">
                              <div class="form-group">
                                  <label class="col-sm-2 control-label"><span class="text-primary">Название шаблона</span></label>
                                    <div class="col-sm-10">
                                      <input name="name" type="text" class="form-control">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Заголовок статьи</label>
                                    <div class="col-sm-10">
                                      <input name="title" type="text" class="form-control">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">URL</label>
                                    <div class="col-sm-10">
                                      <input name="url" type="text" class="form-control">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Тип текста</label>
                                    <div class="col-sm-10">
                                      <select name="text-type" class="form-control">
                                              <option value="COPYRIGHT">Копирайтинг</option>
                                              <option value="REWRITE">Рерайтинг</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Описание проекта</label>
                                    <div class="col-sm-10">
                                      <input name="project-description" type="text" class="form-control" placeholder="Например, требуется 5 статей на тему микрокредитования">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Категория</label>
                                    <div class="col-sm-10">
                                      <select name="category" class="form-control">
                                              <?php
                                              $categories = SmartDB::get_categories();
                                              foreach ($categories as $category) {
                                                    echo "<option value='$category[service_id]'>$category[name]</option>";
                                              }
                                              ?>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Объем (количество слов)</label>
                                    <div class="col-sm-10">
                                      <select name="length" class="form-control">
                                              <option>150</option>
                                              <option>300</option>
                                              <option>400</option>
                                              <option>500</option>
                                              <option>700</option>
                                              <option>1000</option>
                                              <option>2000</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Исполнители</label>
                                    <div class="col-sm-10">
                                      <select name="writers-type" class="form-control">
                                              <option value="ALL">Все</option>
                                              <option value="PREMIUM">Премиум (от 4.1 до 5 звёзд)</option>
                                              <option value="ELITE">Элитные (от 4.6 до 5 звёзд)</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Ключевые слова</label>
                                    <div class="col-sm-10">
                                       <textarea name="keywords" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Цена</label>
                                    <div class="col-sm-10">
                                       <input name="price" type="text" class="form-control" placeholder="">
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Стиль написания</label>
                                    <div class="col-sm-10">
                                      <select name="text-style" class="form-control">
                                              <option value="FRIENDLY">Дружественный</option>
                                              <option value="PROFESSIONAL">Профессиональный</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Цель статьи</label>
                                    <div class="col-sm-10">
                                       <textarea name="article-purpose" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Специальные инструкции</label>
                                    <div class="col-sm-10">
                                       <textarea name="special-instructions" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-sm-2 control-label">Только для указанных исполнителей</label>
                                    <div class="col-sm-10">
                                       <textarea name="writers-list" class="form-control" rows="3"></textarea>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <input type="hidden" name="action-module" value="template" />
                                      <input type="hidden" name="action-method" value="add-template" />
                                      <button type="submit" class="btn btn-default">Добавить</button>
                                    </div>
                              </div>
                            </div>
                    </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">Переменные</div>
                <div class="panel-body" id="vars-panel">
                    <button class="btn btn-default btn-lg" id="add-var-button">Добавить переменную</button>
                </div>
            </div>
        </div>
    </form>
</div>