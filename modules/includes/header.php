<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/img/favicon.ico">
    <title>Articles System</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="/">Articles System</a>
	</div>
	<div class="collapse navbar-collapse">
	  <ul class="nav navbar-nav">
                <?php $module = Controller::get_first_parameter(); ?>
                <li class="dropdown<?php if ($module == "projects" || $module == "archive") echo ' active'; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-book"></span> Проекты <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="/"><span class="glyphicon glyphicon-play-circle"></span> Список текущих проектов</a></li>
                      <li><a href="/archive"><span class="glyphicon glyphicon-calendar"></span> Архив проектов</a></li>
                    </ul>
                </li>
                <li<?php if ($module == "templates") echo ' class="active"'; ?>><a href="/templates"><span class="glyphicon glyphicon-retweet"></span> Шаблоны</a></li>
		<li<?php if ($module == "writers") echo ' class="active"'; ?>><a href="/writers"><span class="glyphicon glyphicon-thumbs-up"></span> Избранные писатели</a></li>
		<li<?php if ($module == "categories") echo ' class="active"'; ?>><a href="/categories"><span class="glyphicon glyphicon-tags"></span> Категории</a></li>
	  </ul>
	</div>
  </div>
</div>