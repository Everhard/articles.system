<div class="container">

	<?php Controller::show_message(); ?>
	<?php
		$article = new Article(Controller::get_second_parameter());
		$project = new Project($article->get_project_id());
		$category = new Category($article->get_category());
	?>

	<div class="panel panel-default">
            <div class="panel-heading">Проект <strong><a href="/project/<?php echo $project->get_id(); ?>"><?php echo $project->get_name(); ?></a></strong> / Статья <strong><a href="/view-article/<?php echo $article->get_id(); ?>"><?php echo $article->get_title(); ?></a></strong></div>
		<div class="panel-body">
                    <form method="post">
			<div class="row">
				<div class="col-lg-6">
                                     <div class="text-justify well">
                                         <textarea name="formatted-text" id="formatted-textarea" class="form-control"><?php echo $article->get_formatted_text() ? $article->get_formatted_text() : $article->get_text(); ?></textarea>
                                     </div>
				</div>
				<div class="col-lg-6">
                                    <div id="article" class="text-justify well">
					<h3>
                                                <?php echo $article->get_title(); ?><br />
                                                <small><?php echo $article->get_article_title(); ?></small>
                                        </h3>
                                        <?php echo $article->get_text(); ?>
                                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<p>
                                            <input type="hidden" name="action-module" value="articles" />
                                            <input type="hidden" name="action-method" value="update-formatted-text" />
                                            <input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
                                            <input class='btn btn-primary' type="submit" value="Сохранить" />
					</p>
				</div>
				<div class="col-lg-6">
					<p class="text-right">
                                            <a class='btn btn-default' href="/view-article/<?php echo $article->get_id(); ?>">Отмена</a>
					</p>
				</div>
			</div>
                    </form>
		</div>
	</div>
</div>

<?php if (!$article->get_status()) { ?>
<form id="push-article-request" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="push-request-article" />
	<input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
</form>
<?php } ?>

<?php if (in_array($article->get_status(), array("open", "in_use"))) { ?>
<form id="check-article-status" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="check-article-status" />
	<input type="hidden" name="service-project-id" value="<?php echo $article->get_service_project_id(); ?>" />
</form>
<?php } ?>

<?php if ($article->get_status() == "available") { ?>
<form id="approve-article" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="approve-article" />
	<input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
</form>
<?php } ?>