<div class="container">

	<?php Controller::show_message(); ?>
	<?php $project = new Project(Controller::get_second_parameter()); ?>

	<div class="panel panel-default">
		<div class="panel-heading">Проект <strong><?php echo $project->get_name(); ?></strong> / Статьи</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-6">
					<a class='btn btn-default' href="/request-article/<?php echo $project->get_id(); ?>">Заказать статью</a>
				</div>
				<div class="col-lg-6 text-right">
					<a class='btn btn-default' id="check-articles-status-button" href="#">Проверить статусы</a>
				</div>
			</div>
		</div>
		<table class="table table-striped" id="articles-list-table">
			<thead>
				<th>Заголовок</th>
				<th>Категория</th>
				<th>Статус</th>
				<th>Управление</th>
			</thead>
			<tbody>
<?php
$articles = $project->get_articles();
if (count($articles) > 0) {
	foreach ($articles as $article) {
		$category = new Category($article->get_category());
		echo "<tr>
					<td>
						<p><a href='/view-article/".$article->get_id()."'>".$article->get_title()."</a></p>
						<p><small class='text-muted'>".$article->get_url()."</small></p>
					</td>
					<td>".$category->get_name()."</td>
					<td>".$article->get_human_status()."</td>
					<td>
						<a title='Изменить статью' class='btn btn-default btn-xs' href='/edit-article/".$article->get_id()."'><span class='glyphicon glyphicon-edit'></span></a>
						<button title='Удалить статью' class='btn btn-default btn-xs delete-article-button' data-id='".$article->get_id()."'><span class='glyphicon glyphicon-remove'></span></button>
					</td>
				</tr>";
	}
} else echo "<tr><td colspan='4' class='text-center'>Нет статей.</td></tr>";
?>
			</tbody>
		</table>
                <div class="panel-footer text-right">
                    <?php if (!$project->get_archived()) { ?><button class="btn btn-default" id="move-to-archive-button">Переместить в архив</button><?php }
                    else { ?><button class="btn btn-default" id="move-from-archive-button">Вернуть из архива</button><?php } ?>
                </div>
	</div>
</div>

<form id="check-articles-status" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="check-articles-status" />
	<input type="hidden" name="project-id" value="<?php echo $project->get_id(); ?>" />
</form>

<form id="delete-article" method="post">
	<input type="hidden" name="action-module" value="articles" />
	<input type="hidden" name="action-method" value="delete-article" />
	<input type="hidden" name="article-id" value="<?php echo $article->get_id(); ?>" />
</form>

<?php if (!$project->get_archived()) { ?>
<form id="move-to-archive" method="post">
	<input type="hidden" name="action-module" value="projects" />
	<input type="hidden" name="action-method" value="move-to-archive" />
	<input type="hidden" name="project-id" value="<?php echo $project->get_id(); ?>" />
</form>
<?php } else { ?>
<form id="move-from-archive" method="post">
	<input type="hidden" name="action-module" value="projects" />
	<input type="hidden" name="action-method" value="move-from-archive" />
	<input type="hidden" name="project-id" value="<?php echo $project->get_id(); ?>" />
</form>
<?php } ?>
